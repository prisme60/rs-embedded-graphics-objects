use embedded_graphics::{
    coord::Coord, icoord, pixelcolor::PixelColor, prelude::*, text_12x16, text_6x12, text_6x8,
    text_8x16, transform::Transform, Drawing,
};
use std::io::Write;

pub enum TextSize {
    Size6x8,
    Size6x12,
    Size8x16,
    Size12x16,
}

pub struct DigitalClock {
    text_size: TextSize,
    coord: Coord,
}

impl DigitalClock {
    pub fn new(text_size: TextSize, coord: (i32, i32)) -> Self {
        DigitalClock {
            text_size,
            coord: icoord!(coord.0, coord.1),
        }
    }

    pub fn set_text_size(&mut self, text_size: TextSize) {
        self.text_size = text_size;
    }

    pub fn draw<D, P>(
        &self,
        display: &mut D,
        hours: usize,
        minutes: usize,
        seconds: usize,
        separator: bool,
    ) where
        D: Drawing<P>,
        P: PixelColor,
    {
        let mut buf = [b' ' as u8; 8];
        if separator {
            write!(&mut buf[..], "{:02}:{:02}:{:02}", hours, minutes, seconds)
                .expect("Can't write");
        } else {
            write!(&mut buf[..], "{:02} {:02} {:02}", hours, minutes, seconds)
                .expect("Can't write");
        }
        let s = std::str::from_utf8(&buf).expect("Buffer to small to build time str");

        match self.text_size {
            TextSize::Size6x8 => display.draw(text_6x8!(s).translate(self.coord)),
            TextSize::Size6x12 => display.draw(text_6x12!(s).translate(self.coord)),
            TextSize::Size8x16 => display.draw(text_8x16!(s).translate(self.coord)),
            TextSize::Size12x16 => display.draw(text_12x16!(s).translate(self.coord)),
        };
    }
}
