use std::f32;

use embedded_graphics::{
    coord::Coord,
    drawable::{Drawable, Pixel},
    egcircle, egline, icoord,
    pixelcolor::PixelColor,
    primitives::{circle::CircleIterator, line::LineIterator},
    style::Style,
    transform::Transform,
};

pub struct AnalogClock<C: PixelColor> {
    rayon: usize,
    needle_h_m_s_size: (usize, usize, usize),
    graduation_size: usize,
    coord: Coord,
    hours: usize,
    minutes: usize,
    seconds: usize,
    _style: Style<C>,
}

impl<C> AnalogClock<C>
where
    C: PixelColor,
{
    pub fn new(
        rayon: usize,
        needle_h_m_s_size: (usize, usize, usize),
        graduation_size: usize,
    ) -> Self {
        AnalogClock {
            rayon,
            needle_h_m_s_size,
            graduation_size,
            coord: icoord!(0, 0),
            hours: 0,
            minutes: 0,
            seconds: 0,
            _style: Style::default(),
        }
    }

    pub fn set_time(&mut self, hours: usize, minutes: usize, seconds: usize) {
        self.hours = hours;
        self.minutes = minutes;
        self.seconds = seconds;
    }

    fn graduation_h_line(&self, hour: usize) -> LineIterator<C> {
        let hour_f = hour as f32 / 6_f32;
        let length = (self.rayon - self.graduation_size) as f32;
        let rayon = (self.rayon - 1) as f32;
        let rot = (hour_f - 0.5) * f32::consts::PI;
        let sin_cos = rot.sin_cos();
        egline!(
            ((length * sin_cos.1) as i32, (length * sin_cos.0) as i32),
            ((rayon * sin_cos.1) as i32, (rayon * sin_cos.0) as i32),
            stroke = Some(1u8.into())
        )
        .translate(self.coord)
        .into_iter()
    }

    fn clock_needle(&self, hour_minute_position: usize, needle_size: usize) -> LineIterator<C> {
        let rot = (hour_minute_position as f32 - 15_f32) * f32::consts::PI / 30_f32;
        let f = rot.sin_cos();
        egline!(
            (0, 0),
            (
                (needle_size as f32 * f.1) as i32,
                (needle_size as f32 * f.0) as i32
            ),
            stroke = Some(1u8.into())
        )
        .translate(self.coord)
        .into_iter()
    }

    fn clock_needles(&self, hours: usize, minutes: usize, seconds: usize) -> [LineIterator<C>; 3] {
        [
            self.clock_needle(seconds, self.needle_h_m_s_size.0),
            self.clock_needle(minutes, self.needle_h_m_s_size.1),
            self.clock_needle(hours * 5 + minutes / 12, self.needle_h_m_s_size.2),
        ]
    }

    fn clock_circle(&self) -> CircleIterator<C> {
        egcircle!(
            (self.coord.0, self.coord.1),
            (self.rayon - 1) as u32,
            stroke = Some(1u8.into())
        )
        .into_iter()
    }
}

impl<'a, C> IntoIterator for &'a AnalogClock<C>
where
    C: PixelColor,
{
    type Item = Pixel<C>;
    type IntoIter = AnalogClockIterator<C>;

    fn into_iter(self) -> Self::IntoIter {
        AnalogClockIterator {
            lines_needles_iterator: self.clock_needles(self.hours, self.minutes, self.seconds),
            lines_marks_iterator: [
                self.graduation_h_line(0),
                self.graduation_h_line(1),
                self.graduation_h_line(2),
                self.graduation_h_line(3),
                self.graduation_h_line(4),
                self.graduation_h_line(5),
                self.graduation_h_line(6),
                self.graduation_h_line(7),
                self.graduation_h_line(8),
                self.graduation_h_line(9),
                self.graduation_h_line(10),
                self.graduation_h_line(11),
            ],
            circle_iterator: self.clock_circle(),
            index_needles: 0,
            index_marks: 0,
        }
    }
}

/// Pixel iterator for each pixel in the rect border
#[derive(Debug, Clone, Copy)]
pub struct AnalogClockIterator<C: PixelColor>
where
    C: PixelColor,
{
    lines_needles_iterator: [LineIterator<C>; 3],
    lines_marks_iterator: [LineIterator<C>; 12],
    circle_iterator: CircleIterator<C>,
    index_needles: usize,
    index_marks: usize,
}

impl<C> Iterator for AnalogClockIterator<C>
where
    C: PixelColor,
{
    type Item = Pixel<C>;

    fn next(&mut self) -> Option<Self::Item> {
        while self.index_needles < self.lines_needles_iterator.len() {
            if let Some(needle_point) = self.lines_needles_iterator[self.index_needles].next() {
                return Some(needle_point);
            }
            self.index_needles += 1;
        }
        while self.index_marks < self.lines_marks_iterator.len() {
            if let Some(mark_point) = self.lines_marks_iterator[self.index_marks].next() {
                return Some(mark_point);
            }
            self.index_marks += 1;
        }
        self.circle_iterator.next()
    }
}

impl<C> Drawable for AnalogClock<C> where C: PixelColor {}

impl<C> Transform for AnalogClock<C>
where
    C: PixelColor,
{
    fn translate(&self, by: Coord) -> Self {
        Self {
            coord: self.coord + by,
            ..*self
        }
    }

    fn translate_mut(&mut self, by: Coord) -> &mut Self {
        self.coord += by;

        self
    }
}
